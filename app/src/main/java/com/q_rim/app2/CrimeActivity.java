package com.q_rim.app2;

import android.support.v4.app.Fragment;

import java.util.UUID;

public class CrimeActivity extends SingleFragmentActivity {

  @Override
  protected Fragment createFragment() {
//    return new CrimeFragment();

    // Using newInstance(UUID)  - for attaching Args to Fragment <10.6>
    UUID crimeId = (UUID)getIntent().getSerializableExtra(CrimeFragment.EXTRA_CRIME_ID);
    return CrimeFragment.newInstance(crimeId);
  }
}