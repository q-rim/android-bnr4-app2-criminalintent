package com.q_rim.app2;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;

import java.util.UUID;

public class CrimeFragment extends Fragment {
  public static final String EXTRA_CRIME_ID = "com.q_rim.android.app2.crime_id";

  private Crime crime;
  private EditText title_EditText;
  private Button dateButton;
  private CheckBox solvedCheckBox;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
//    crime = new Crime();

    //UUID crimeId = (UUID)getActivity().getIntent().getSerializableExtra(EXTRA_CRIME_ID);
    UUID crimeId = (UUID)getArguments().getSerializable(EXTRA_CRIME_ID);    // Used for attaching Args to Fragment.  <10.7>
    crime = CrimeLabSingleton.get(getActivity()).getCrime(crimeId);
  }

  // Inflate layout for the fragment's view and return inflated View to the hosting activity.
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
    View v = inflater.inflate(R.layout.fragment_crime, parent, false);

    dateButton = (Button)v.findViewById(R.id.date_Button);
    dateButton.setText(crime.getDate());
    dateButton.setEnabled(false);

    title_EditText = (EditText)v.findViewById(R.id.crime_title_EditText);
    title_EditText.setText(crime.getTitle());
    title_EditText.addTextChangedListener(new TextWatcher() {

      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {
        crime.setTitle(s.toString());
      }

      @Override
      public void afterTextChanged(Editable s) {

      }
    });

    solvedCheckBox = (CheckBox)v.findViewById(R.id.solved_CheckBox);
    solvedCheckBox.setChecked(crime.isSolved());
    solvedCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        crime.setSolved(isChecked);       Log.d("-----crime.isSolved", ""+crime.isSolved());
      }
    });

    return v;
  }

  // Attaching Args to Fragments <10.5>
  public static CrimeFragment newInstance(UUID crimeId) {
    Bundle args = new Bundle();
    args.putSerializable(EXTRA_CRIME_ID, crimeId);

    CrimeFragment fragment = new CrimeFragment();
    fragment.setArguments(args);

    return fragment;
  }
}