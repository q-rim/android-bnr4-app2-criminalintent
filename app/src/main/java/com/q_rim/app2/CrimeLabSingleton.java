package com.q_rim.app2;

import android.content.Context;

import java.util.ArrayList;
import java.util.UUID;

public class CrimeLabSingleton {
  // Singleton is used when there is only one instance of this Object created.
  private ArrayList<Crime> crimes;

  private static CrimeLabSingleton crimeLab;
  private Context appContext;

  private CrimeLabSingleton(Context appContext) {
    appContext = appContext;
    crimes = new ArrayList<Crime>();
    for (int i = 0; i < 100; i++) {
      Crime c = new Crime();
      c.setTitle("Crime #: " +i);
      c.setSolved(i % 2 == 0);    // Every other one
      crimes.add(c);
    }
  }

  public static CrimeLabSingleton get(Context context) {
    if (crimeLab == null) {
      crimeLab = new CrimeLabSingleton(context.getApplicationContext());
    }
    return crimeLab;
  }

  // getting ArrayList of crime
  public ArrayList<Crime> getCrimes() {
    return crimes;
  }

  // getting a single crime
  public Crime getCrime(UUID id) {
    for (Crime c : crimes) {
      if (c.getId().equals(id)) {   // if crime id matches
        return c;
      }
    }
    return null;                    // if no match found
  }
}
